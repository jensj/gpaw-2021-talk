import sys
from pathlib import Path
from collections import defaultdict
import matplotlib.pyplot as plt

if 0:
    suffixes = ['.py', '.c', '.h', '.rst', '.sh']
    root = Path(sys.argv[1])
    nfiles = defaultdict(int)
    nlines = defaultdict(int)
    for f in root.glob('**/*'):
        if '.git/' in str(f) or not f.is_file():
            continue
        suffix = f.suffix
        if suffix not in {'.png'}:
            try:
                lines = len(f.read_text().splitlines())
            except UnicodeDecodeError:
                print(f)
                lines = 0
        else:
            lines = 0
        if suffix not in suffixes:
            suffix = '.???'

        if suffix == '.py' and 'gpaw/test/' in str(f):
            suffix = '.py (test)'

        nfiles[suffix] += 1
        nlines[suffix] += lines
else:
    nfiles = {'.???': 83,
              '.rst': 235,
              '.py': 882,
              '.sh': 32,
              '.c': 50,
              '.py (test)': 584,
              '.h': 13}
    nlines = {'.???': 34955,
              '.rst': 29126,
              '.py': 140047,
              '.sh': 1651,
              '.c': 16609,
              '.py (test)': 36394,
              '.h': 739}


def make_pies():
    print(nfiles, nlines)
    labels = sorted(nfiles)
    files = [nfiles[label] for label in labels]
    lines = [nlines[label] for label in labels]
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.pie(files, labels=labels, shadow=True)
    ax2.pie(lines, labels=labels, shadow=True)
    ax1.set_title(f'Files: {sum(files)}')
    ax2.set_title(f'Lines: {sum(lines)}')
    plt.tight_layout()
    # plt.show()
    plt.savefig('slides/pies.png')


if __name__ == '__main__':
    make_pies()
