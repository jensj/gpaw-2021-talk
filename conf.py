import sys
sys.path.append('.')
project = 'gpaw-2021'
copyright = '2021, Jens Jørgen Mortensen'
author = 'Jens Jørgen Mortensen'
extensions = ['sphinx.ext.extlinks',
              'sphinx.ext.intersphinx',
              'make_slides']
extlinks = {'doi': ('https://doi.org/%s', 'doi:'),
            'git': ('https://gitlab.com/gpaw/gpaw/-/blob/master/%s', '')}
templates_path = ['_templates']
exclude_patterns = ['_build', 'README.rst', 'venv']
html_theme = 'bizstyle'
html_theme_options = {'nosidebar': True,
                      'navigation_with_keys': True}
intersphinx_mapping = {'gpaw': ('https://wiki.fysik.dtu.dk/gpaw', None),
                       'ase': ('https://wiki.fysik.dtu.dk/ase', None)}
default_role = 'git'
html_style = 'my.css'
html_static_path = ['_static']
