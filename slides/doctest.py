def func(x: int) -> int:
    """Silly function.

    >>> func(1)
    2
    """
    return x + 1
