from myqueue.workflow import run

def workflow():
    """Run jellium tutorial and create fig2.png."""
    run(script='bulk.py', cores=4)
    with run(script='surface.py', cores=4, tmax='1h'):
        with run(script='sigma.py'):
            run(script='fig2.py')
