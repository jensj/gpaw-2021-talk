from gpaw import GPAW

def test_stuff(in_tmp_dir, gpw_files):
    """Test some feature that needs a GPAW object.

    Also write some files and make a mess.  Pytest will clean up.
    """
    calc = GPAW(gpw_files['bcc_li_lcao'])
    ...
