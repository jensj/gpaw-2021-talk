from ase.build import molecule
from gpaw import GPAW
atoms = molecule('H2O')
atoms.center(vacuum=3.0)
atoms.calc = GPAW(mode=dict(name='pw',
                            ecut=400),
                  xc='PBE')
energy = atoms.get_potential_energy()
