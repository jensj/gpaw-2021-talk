==============================
A tour of the GPAW source code
==============================

Slides
======

.. toctree::
   :glob:

   slides/slide-*

.. toctree::
   :hidden:

   slides/talk

:ref:`All slides in one <slides>`
