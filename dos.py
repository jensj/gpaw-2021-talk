import subprocess

import numpy as np
from ase.spectrum.doscollection import GridDOSCollection
from ase.spectrum.dosdata import GridDOSData
from gpaw import GPAW
from gpaw.dos import DOSCalculator


def make_dos():
    subprocess.run('ase build -x diamond -a 5.43 Si si.xyz', shell=True)
    subprocess.run(
        'gpaw run -p "mode=pw,kpts={density:4},xc=PBE" -w si.gpw si.xyz',
        shell=True)
    calc = GPAW('si.gpw')
    doscalc = DOSCalculator.from_calculator(calc)
    emin = -10
    emax = 2
    energies = doscalc.get_energies(emin, emax, npoints=400)
    dosobjs = GridDOSCollection([], energies)
    projs = [('Si-s', [(0, 0, None), (1, 0, None)]),
             ('Si-p', [(0, 1, None), (1, 1, None)])]
    for label, contributions in projs:
        dos = np.zeros_like(energies)
        for a, l, m in contributions:
            dos += doscalc.raw_pdos(energies,
                                    a, l, m, spin=0, width=0.0)
        dosobjs += GridDOSData(energies, dos, {'label': label})
    ylabel = 'DOS [electrons/eV]'
    ax = dosobjs.plot()
    ax.set_xlabel(r'$\epsilon-\epsilon_F$ [eV]')
    ax.set_ylabel(ylabel)
    import matplotlib.pyplot as plt
    plt.savefig('slides/dos.png')


if __name__ == '__main__':
    make_dos()
